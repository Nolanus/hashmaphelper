package hashMap;
/**
 * A class for testing a hash function
 * @author Sebastian Fuss
 * @version 0.1
 */
public class HashMapHelper {

	private int nMax = 0;
	private Integer[] hashMap;
	private boolean verbose;
	

	
	/**
	 * Create a new HashMapHelper object
	 * @param nMax The size of the hashMap
	 * @param verbose Set true to activate verbose-mode
	 */
	public HashMapHelper(int nMax, boolean verbose){
		super();
		this.nMax = nMax;
		this.verbose = verbose;
		hashMap = new Integer[nMax];
	}
	
	/**
	 * Create a new HashMapHelper object with inactive verbose mode
	 * @param nMax The size of the hashMap
	 */
	public HashMapHelper(int nMax){
		this(nMax, false);
	}

	/**
	 * A hashing function
	 * @param i number of attempt
	 * @param nMax The size of the hashMap
	 * @param k the value to be inserted
	 * @return A hash key for <i>k</i>
	 */
	private int hashFunction(int i, int nMax, int k){
		/* Linear probing */
		//return (k % nMax) + (i-1) % nMax;
		
		/* Quadratic probing */
		return (k % nMax) + (i-1)*(i-1) % nMax;
		
		//return ((k % nMax) + (i-1) * (k + (k %nMax))) % nMax;
		//return (k % nMax + (i-1) * (4 + (k % nMax))) % nMax;
	}
	
	/**
	 * Return a string where <i>val</i> is embedded into spaces to form a string with length <i>spaces</i>
	 * If <i>val</i>'s length is larger than <i>spaces</i>, <i>val</i> is returned.
	 * @param val The string to be suffixed and prefixed with spaces
	 * @param spaces The length of the result string
	 * @return A string with length <i>spaces</i> containing <i>val</i> in the middle
	 */
	private String hOutput(String val, int spaces){
		int puffer = spaces-val.length();
		String oddFiller = "";
		if (puffer > 0){
			if (puffer % 2 == 1){
				oddFiller = " ";
			}
			return multipleOutput(" ", puffer/2) + val + oddFiller + multipleOutput(" ", puffer/2);
		}else{
			return val;
		}
	}
	
	/**
	 * Return a given String multiple times
	 * @param val The String to be returned multiple times
	 * @param amount The amount of repetitions
	 * @return A string containing <i>val</i> <i>amount</i>-times
	 */
	private String multipleOutput(String val, int amount){
		String result = "";
		for (int i = 0; i < amount; i++) {
			result += val;
		}
		return result;
	}
	
	/**
	 * Set the verbose mode. In active verbose mode the hashMap will be
	 * print after every insertion
	 * @param verbose value to verbose mode to
	 */
	public void setVerbose(boolean verbose){
		this.verbose = verbose;
	}
	
	/**
	 * Print the current hashMap to the default output
	 */
	public void printHashMap() {
		System.out.println("\n" + multipleOutput("=", 15) + " Your HashMap " + multipleOutput("=", 15));
		String line1 = "|", line2 = "|", line3 = "|";
		for (int i = 0; i < hashMap.length; i++) {
			int spaces = String.valueOf(i).length() + 2;
			if (hashMap[i] != null){
				int numberSpaces = String.valueOf(hashMap[i]).length() + 2;
				if (numberSpaces > spaces){
					spaces = numberSpaces;
				}
				line3 += hOutput(String.valueOf(hashMap[i]), spaces);
			}else{
				line3 += hOutput("-", spaces);
			}
			line1 += hOutput(String.valueOf(i), spaces) + "|";
			line2 += multipleOutput("-", spaces) + "|";
			line3 += "|";
		}
		System.out.println(line1 + "\n" + line2 + "\n" + line3 + "\n");
	}
	
	/**
	 * Insert items in the hashMap
	 * @param items An array of the integers you want to insert into the hashMap
	 * @exception IllegalArgumentException
	 */
	public void insert(int[] items){
		int collCount = 0; // Collision counter
		if (items.length < nMax){
			int currentHashValue, i = 1;
			for (int item : items) {
				System.out.println("Inserting "+item+":");
				while (i <= nMax) {
					currentHashValue = hashFunction(i, nMax, item);
					System.out.print("   F("+i+", "+nMax+", "+item+") = "+currentHashValue);
					if (hashMap[currentHashValue] == null){
						hashMap[currentHashValue] = item;
						System.out.println(" inserted");
						i = 1;
						break;
					}else if (i == nMax){
						System.out.println(" unable to insert into HashMap");						
					}else{
						collCount++;
						System.out.println(" Hash-Collision!");
					}
					i++;
				}
				if (verbose){
					printHashMap();
				}
			}
		}else{
			throw new IllegalArgumentException("hashMap too small");
		}
		int insertOpts = items.length + collCount;
		System.out.println("\nInsert Operations: "+(insertOpts));
		System.out.println("Hash-Collisions of that: "+collCount);
		printHashMap();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashMapHelper myHasher = new HashMapHelper(13);
		//myHasher.setVerbose(true);
		myHasher.insert(new int[]{13, 1, 2, 3, 26, 40});
	}

}
